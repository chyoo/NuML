#!/usr/bin/env python
import sys, os
from glob import glob
from ROOT import *
import numpy as np
import time

fin = TFile("template.root")
hPulse0 = fin.Get("hPulse0")
hPulse1 = fin.Get("hPulse1")
minT = hPulse0.GetXaxis().GetXmin()
#maxT = hPulse0.GetXaxis().GetXmax()
nT = hPulse0.GetNbinsX()
dt = hPulse0.GetXaxis().GetBinWidth(1)

c = TCanvas("c", "c", 500, 500)
hPulse0.SetLineColor(kRed)
hPulse1.SetLineColor(kBlue)
hPulse0.Draw("hist")
hPulse1.Draw("samehist")
h = hPulse0.Clone()

fNames = []
for fName in sys.argv[1:]:
    if not fName.endswith('.root'): fName += '/*.root'
    fNames.extend(glob(fName))

for fName in fNames:
    f = TFile(fName)
    tree = f.Get("tree")
    for event in tree:
        flag = event.SampleFlag
        wavVals = event.mVcomspDS
        if nT != len(wavVals):
            print("Inconsistent timing bins")
            exit()
        peakT = np.argmax(wavVals)*dt
        maxW = np.max(wavVals)

        h.Reset()
        for i, w in enumerate(wavVals):
            t = i*dt - peakT
            h.Fill(t+0.5*dt, w)
        h.Scale(1./maxW)

        if flag == 0: h.SetLineColor(kBlack)
        else: h.SetLineColor(kGreen+1)
        h.Draw("samehist")
        c.Modified()
        c.Update()

        time.sleep(0.5)

