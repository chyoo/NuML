#!/usr/bin/env python
import sys, os
from glob import glob
import argparse

parser = argparse.ArgumentParser(description='Convert pulse shape root files to hdf')
parser.add_argument('inputFiles', metavar='N', type=str, nargs='+',
                    help='Input file names or directory')
parser.add_argument('-o', '--output', action='store', type=str, required=True, help='output file name')
args = parser.parse_args()

fNames = []
for d in args.inputFiles:
    if d.endswith('.root'): fNames.append(d)
    elif os.path.isdir(d): fNames.extend(glob(d+'/*.root'))

import numpy as np
nT = int(496/0.2)
out_waveforms = np.zeros([0,nT])
out_labels = np.ones(0)
out_psd = np.zeros(0)
out_qmqt = np.zeros(0)

import uproot
import tqdm
for fName in tqdm.tqdm(fNames):
    fin = uproot.open(fName)
    tree = fin['tree']

    out_labels = np.concatenate([out_labels, np.array(tree['SampleFlag'].array())])
    out_waveforms = np.concatenate([out_waveforms, np.array(tree['mVcomspDS'].array())])
    out_psd = np.concatenate([out_psd, np.array(tree['PSD_ref'].array())])
    out_qmqt = np.concatenate([out_qmqt, np.array(tree['qmqt'].array())])

## Flip 1 and 0:
## original data set 0->Michel electron, 1->Fast Neutron
## But ML convention set 0<-Background, 1<-Signal 
out_labels = 1-out_labels

import h5py
kwargs = {'dtype':'f4', 'compression':'lzf'}
chunkSize = min(1024, out_labels.shape[0])
print("Saving output...", end="")
with h5py.File(args.output, 'w', libver='latest', swmr=True) as fout:
    g = fout.create_group('events')
    g.create_dataset('labels', data=out_labels, chunks=(chunkSize,), dtype='i4')
    g.create_dataset('waveforms', data=out_waveforms, chunks=(chunkSize, nT), **kwargs)
    g.create_dataset('psd', data=out_psd, chunks=(chunkSize,), dtype='f4')
    g.create_dataset('qmqt', data=out_qmqt, chunks=(chunkSize,), dtype='f4')

print("Done.")
