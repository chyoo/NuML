#!/usr/bin/env python
import sys, os
if sys.version[0] != '3':
    print("Runs on python3. exit")
    exit()

from glob import glob
import argparse

parser = argparse.ArgumentParser(description='Convert pulse shape root files to hdf')
parser.add_argument('inputFiles', metavar='N', type=str, nargs='+',
                    help='Input file names or directory')
parser.add_argument('-o', '--output', action='store', type=str, required=True, help='output file name')
parser.add_argument('-q', '--quiet', action='store_true', default=False, help='suppress progress bar')
flags = ["ME","FN"]
parser.add_argument('--flag', choices=flags, required=True, help='label name to select')
parser.add_argument('--no-fiducial-cut', dest='nocut', action='store_true', default=False, help='do not apply fiducial cuts (60cm)')
args = parser.parse_args()

fNames = []
for d in args.inputFiles:
    if d.endswith('.root'): fNames.append(d)
    elif os.path.isdir(d): fNames.extend(glob(d+'/*.root'))

import numpy as np
nT = 248
out_meta_shape = np.array([96*2, nT]) ## 96PMT x (high, low gain channel), 248 time bins
out_labels = []
out_fadcs = []
out_isFiducial = []

flagName = args.flag+'Flag'
import uproot
import tqdm
for fName in (fNames if args.quiet else tqdm.tqdm(fNames)):
    fin = uproot.open(fName)
    tree = fin['prod']

    labels = np.array(tree[args.flag+'Flag'].array())

    vtx3 = np.array(tree['recoVertexArray'].array())
    vtxRho = np.hypot(vtx3[:,0], vtx3[:,1])
    vtxZ = vtx3[:,2]
    isFiducial = (vtxRho<0.6) & (np.abs(vtxZ)<0.6)

    fadc = np.array(tree['FADC'].array())
    ## split FADC by board/channels, 8 channels per boards
    fadc_hi = fadc[:, 1: 1+12].reshape([-1,12*8,nT]) ## High gain channels: 1-12
    fadc_lo = fadc[:,13:13+12].reshape([-1,12*8,nT]) ## Low gain channels: 13-24
    #fadc_Veto = fadc[:,25:25+3] ## veto channels: 25,26,27
    fadcs = np.stack([fadc_hi, fadc_lo], axis=2).reshape([-1,12*8*2*nT]) ## (nEvents), (12 boards x 8 channels x 2 gains x time bins)

    if not args.nocut:
        fadcs = fadcs[isFiducial]
        labels = labels[isFiducial]
        isFiducial = isFiducial[isFiducial]

    fadcs = fadcs[labels==1]
    isFiducial = isFiducial[labels==1]
    n = len(isFiducial)
    labels = np.zeros(n) if args.flag == 'ME' else np.ones(n)

    out_labels.append(labels)
    out_fadcs.append(fadcs)
    out_isFiducial.append(isFiducial)

out_labels = np.concatenate(out_labels)
out_fadcs = np.concatenate(out_fadcs)
out_isFiducial = np.concatenate(out_isFiducial)

if len(out_labels) == 0:
    print("@@@ Empty output after event selection. skip writing output file")
    exit()

import h5py
kwargs = {'dtype':'f4', 'compression':'lzf'}
print("Saving output...", end="")
with h5py.File(args.output, 'w', libver='latest', swmr=True) as fout:
    m = fout.create_group('info')
    m.create_dataset('shape', data=out_meta_shape, dtype='i4')

    g = fout.create_group('events')
    g.create_dataset('labels', data=out_labels, chunks=(1,), dtype='i4')
    n = out_meta_shape[0]*min(nT, len(out_labels))
    g.create_dataset('fadcs', data=out_fadcs, chunks=(1, n), **kwargs)
    g.create_dataset('isFiducial', data=out_isFiducial, chunks=(1,), **kwargs)

    print("")
    for name in ['labels', 'fadcs', 'isFiducial']:
        print('>', name, g[name].shape)

print("Done.")
