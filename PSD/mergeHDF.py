#!/usr/bin/env python
import sys, os
from glob import glob
import argparse

parser = argparse.ArgumentParser(description='Convert pulse shape root files to hdf')
parser.add_argument('inputFiles', metavar='N', type=str, nargs='+',
                    help='Input file names or directory')
parser.add_argument('-o', '--output', action='store', type=str, required=True, help='output file name')
parser.add_argument('-q', '--quiet', action='store_true', default=False, help='supress progress bar')
args = parser.parse_args()

fNames = []
for d in args.inputFiles:
    if d.endswith('.h5'): fNames.append(d)
    elif os.path.isdir(d): fNames.extend(glob(d+'/*.h5'))

import numpy as np
out_meta_shape = None
out_labels = []
out_waveforms = []
out_isFiducial = []

import h5py
import tqdm
for fName in fNames if args.quiet else tqdm.tqdm(fNames):
    fin = h5py.File(fName, 'r')
    if len(fin['events/labels']) == 0: continue
    
    if out_meta_shape is None: out_meta_shape = fin['info/shape'][()]
    elif np.any(out_meta_shape != fin['info/shape'][()]):
        print("!!! Trying to merge files with different shape, A=", out_meta_shape.shape, "B=", fin['info/shape'].shape)
        continue

    out_labels.append(fin['events/labels'][()])
    out_waveforms.append(fin['events/waveforms'][()])
    out_isFiducial.append(fin['events/isFiducial'][()])

kwargs = {'dtype':'f4', 'compression':'lzf'}
print("Saving output...", end="")
with h5py.File(args.output, 'w', libver='latest', swmr=True) as fout:
    m = fout.create_group('info')
    m.create_dataset('shape', data=out_meta_shape)

    g = fout.create_group('events')
    g.create_dataset('labels', data=np.concatenate(out_labels), chunks=(1,), dtype='i4')
    l = out_waveforms[0].shape[-1]
    g.create_dataset('waveforms', data=np.concatenate(out_waveforms), chunks=(1, l), **kwargs)
    g.create_dataset('isFiducial', data=np.concatenate(out_isFiducial), chunks=(1,), dtype='i4')

print("Done.")
