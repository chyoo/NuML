#!/usr/bin/env python
import sys, os
if sys.version[0] != '3':
    print("Runs on python3. exit")
    exit()

from glob import glob
import argparse

parser = argparse.ArgumentParser(description='Convert pulse shape root files to hdf')
parser.add_argument('inputFiles', metavar='N', type=str, nargs='+',
                    help='Input file names or directory')
parser.add_argument('-o', '--output', action='store', type=str, required=True, help='output file name')
parser.add_argument('--nocut', action='store_true', default=False, help='do not apply cuts')
parser.add_argument('-q', '--quiet', action='store_true', default=False, help='supress progress bar')
parser.add_argument('-c', '--channel', choices=[1,96], default=1, type=int, help='number of channels')
parser.add_argument('--label', choices=[-1,0,1], default=-1, type=int, help='label to select. -1 default is not to apply selection cut.')
args = parser.parse_args()

fNames = []
for d in args.inputFiles:
    if d.endswith('.root'): fNames.append(d)
    elif os.path.isdir(d): fNames.extend(glob(d+'/*.root'))

import numpy as np
nT = int(496/0.2)
nCh = args.channel
out_meta_shape = np.ndarray([nCh, nT])
out_labels = []
out_waveforms = []
out_isFiducial = []

import uproot
import tqdm
for fName in fNames if args.quiet else tqdm.tqdm(fNames):
    fin = uproot.open(fName)
    tree = fin['prod']

    labels = np.array(tree['MEFlag'].array())
    #flagFN = np.array(tree['FNFlag'].array())

    vtx3 = np.array(tree['recoVertexArray'].array())
    vtxRho2 = vtx3[:,0]*vtx3[:,0] + vtx3[:,1]*vtx3[:,1]
    isFiducial = (vtxRho2 < 0.6**2) & (np.abs(vtx3[:,2]) < 0.6)

    if nCh == 1 and 'mVcomspDS' in tree.keys():
        waveforms = np.array(tree['mVcomspDS'].array())
    else:
        ## For the 96 channel array, shape is (n, c, w)
        waveforms = np.array(tree['mVcomspAlign'].array())
    if not args.nocut:
        labels = labels[isFiducial]
        waveforms = waveforms[isFiducial]
        isFiducial = isFiducial[isFiducial]

    if args.label != -1:
        waveforms = waveforms[labels==args.label]
        isFiducial = isFiducial[labels==args.label]
        labels = labels[labels==args.label]

    out_labels.append(labels)
    out_waveforms.append(waveforms)
    out_isFiducial.append(isFiducial)

out_labels = np.concatenate(out_labels)
out_waveforms = np.concatenate(out_waveforms)
out_waveforms = out_waveforms.reshape([-1, nCh*nT])
out_isFiducial = np.concatenate(out_isFiducial)

if len(out_labels) == 0:
    print("@@@ Empty output after event selection. skip writing output file")
    exit()

import h5py
kwargs = {'dtype':'f4', 'compression':'lzf'}
print("Saving output...", end="")
with h5py.File(args.output, 'w', libver='latest', swmr=True) as fout:
    m = fout.create_group('info')
    m.create_dataset('shape', data=out_meta_shape, dtype='i4')

    g = fout.create_group('events')
    g.create_dataset('labels', data=out_labels, chunks=(1,), dtype='i4')
    g.create_dataset('waveforms', data=out_waveforms, chunks=(1, nCh*nT), **kwargs)
    g.create_dataset('isFiducial', data=out_isFiducial, chunks=(1,), **kwargs)

print("Done.")
