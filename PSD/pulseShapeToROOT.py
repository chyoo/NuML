#!/usr/bin/env python
import sys, os, argparse
from ROOT import *
import numpy as np
from array import array
import ctypes

minT, maxT, dt = 0.-100, 496.-100, 0.2
nT = int((maxT-minT)/dt)
nCat0, nCat1 = 0., 0.
nBins = 2480//(5*16)
epsilon = 1e-9 ## this is needed to avoid funny binning problem

parser = argparse.ArgumentParser()
parser.add_argument('-o', '--output', action='store', type=str, required=True, help='Path to output file')
parser.add_argument('input', metavar='N', type=str, nargs='+', help='input files')
args = parser.parse_args()

fout = TFile(args.output, "recreate")
fout.cd()
hPulse0 = TH1D("hPulse0", "pulse of cat 0;Time (ns);Entries", nT, minT, maxT)
hPulse1 = TH1D("hPulse1", "pulse of cat 1;Time (ns);Entries", nT, minT, maxT)

hPeak0 = TH1D("hPeak0", "Time at peak;Time (ns);Events", 200, 110-50, 110+50)
hPeak1 = TH1D("hPeak1", "Time at peak;Time (ns);Events", 200, 110-50, 110+50)

outTree = TTree("tree", "tree")
out_label = ctypes.c_int()
out_waveform = array('d', np.zeros(nT-100)) ## we cut 50 points
outTree.Branch("label", out_label, "label/I")
outTree.Branch("waveform", out_waveform, "waveform[%d]/D" % (nT-100))

hPulseTmp = TH1D("hPulseTmp", "pulse shape rebinned;Time (ns);Entries", nBins, minT+dt*nBins, maxT-dt*nBins)
out_wfs = []
for i in range(hPulseTmp.GetNbinsX()):
    v = ctypes.c_double()
    outTree.Branch("wf%d" % i, v, "wf%d/D" % i)
    out_wfs.append(v)

from glob import glob
fNames = []
for dName in args.input:
    if dName.endswith('.root'): fNames.append(fName)
    elif os.path.isdir(dName): fNames.extend(glob(dName+'/*.root'))
for fName in fNames:
    print("Opening", fName, end="\r")
    #print "Opening", fName, "\r",
    f = TFile(fName)
    tree = f.Get("tree")
    for event in tree:
        flag = event.SampleFlag
        wavVals = event.mVcomspDS
        if nT != len(wavVals):
            print("Inconsistent timing bins")
            exit()
        if np.max(wavVals) == 0: continue
        peakI = np.argmax(wavVals)
        peakT = peakI*dt
        peakV = wavVals[peakI]
        wavVals = np.divide(wavVals, peakV)

        hPulseTmp.Reset()
        if flag == 0: ## 0 is Michell electron
            nCat0 += 1
            for i, w in enumerate(wavVals):
                t = i*dt - peakT
                hPulse0.Fill(t+epsilon, w)
                hPulseTmp.Fill(t, w)
            hPeak0.Fill(peakT+epsilon)
        else: ## 1 is Fast Neutron
            nCat1 += 1
            for i, w in enumerate(wavVals):
                t = i*dt - peakT
                hPulse1.Fill(t+epsilon, w)
                hPulseTmp.Fill(t, w)
            hPeak1.Fill(peakT+epsilon)
        #hPulseTmp.Scale(1./hPulseTmp.GetMaximum())

        out_label.value = flag
        for i in range(nT-100): out_waveform[i] = wavVals[i+50]
        #for i in range(20):
        #    k = i*(nT-100)/20
        #    out_wfs[i].value = wavVals[k+50]
        for i in range(hPulseTmp.GetNbinsX()):
            out_wfs[i].value = hPulseTmp.GetBinContent(i+1)
        outTree.Fill()

#scaleCat0 = 1./max(hPulse0.GetBinContent(i) for i in range(1, nT+1))
#scaleCat1 = 1./max(hPulse1.GetBinContent(i) for i in range(1, nT+1))

c1 = TCanvas("c1", "c1", 500, 500)
hPulse0.SetLineColor(kRed)
hPulse1.SetLineColor(kBlue)
#hPulse0.Scale(scaleCat0)
#hPulse1.Scale(scaleCat1)
hPulse0.Scale(1./nCat0)
hPulse1.Scale(1./nCat1)
hPulse0.Draw("hist")
hPulse1.Draw("samehist")

c2 = TCanvas("c2", "c2", 500, 500)
hPeak0.SetLineColor(kRed)
hPeak1.SetLineColor(kBlue)
hPeak0.Scale(1./nCat0)
hPeak1.Scale(1./nCat1)
hPeak0.Draw("hist")
hPeak1.Draw("samehist")

############
fout.cd()

hPulse0.SetDirectory(fout)
hPulse1.SetDirectory(fout)
hPulse0.Write()
hPulse1.Write()

hPeak0.SetDirectory(fout)
hPeak1.SetDirectory(fout)
hPeak0.Write()
hPeak1.Write()

outTree.SetDirectory(fout)
outTree.Write()

