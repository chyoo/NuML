#!/usr/bin/env python
import pandas as pd
import numpy as np
import sys, os
from ROOT import *
gStyle.SetOptTitle(False)
gStyle.SetOptStat(False)
gStyle.SetPadTopMargin(0.05)
gStyle.SetPadBottomMargin(0.12)
gStyle.SetPadLeftMargin(0.12)
gStyle.SetPadRightMargin(0.05)
gStyle.SetTitleOffset(1.2, "X")
gStyle.SetTitleOffset(1.4, "Y")
gStyle.SetTitleSize(0.04, "X")
gStyle.SetTitleSize(0.04, "Y")

marginLeft, marginTop, marginRight = 0.10, 0.05, 0.05
varNames = ['loss', 'val_loss', 'acc', 'val_acc']
varTitle = ["Training loss", "Validation loss",
            "Training accuracy", "Validation accuracy"]

grps = {}
maxEpoch = 0
minLoss = [1e9,]*len(varNames)
maxLoss = [0]*len(varNames)
for dName in sys.argv[:]:
    if not os.path.isdir(dName): continue
    if not os.path.exists(dName+"/train.csv"): continue

    df = pd.read_csv(dName+"/train.csv")
    epoch = len(df)
    maxEpoch = max(epoch, maxEpoch)

    gs = []
    for i, varName in enumerate(varNames):
        l = df[varName].to_numpy()
        x = np.linspace(1, epoch, epoch)[~np.isnan(l)]

        g = TGraph(len(x), x, l[~np.isnan(l)]) if len(x) > 0 else TGraph()
        g.SetTitle(dName)
        g.SetEditable(False)
        gs.append(g)

        minLoss[i] = min([df[varName].min(), minLoss[i]])
        maxLoss[i] = max([df[varName].max(), maxLoss[i]])

    grps[dName] = gs

if len(grps) == 0:
    print("No valid loss curve")
    exit()

hFrames = []
hLegends = []
for i, varName in enumerate(varNames):
    h = TH1F("hFrame%d" % i, ";Epoch;"+varTitle[i], maxEpoch+1, 0, maxEpoch+1)

    h.SetMinimum(minLoss[i]*0.8)
    h.SetMaximum(maxLoss[i]*1.2)
    #h.SetMinimum(minLoss*0.5)
    #h.SetMaximum(maxLoss*10.0)

    l = TLegend(marginLeft+0.02, 1-marginTop-0.04*len(grps)-0.03,
                1-marginRight-0.02, 1-marginTop-0.03, "", "NDC")
    l.SetBorderSize(0)
    l.SetFillStyle(0)

    hFrames.append(h)
    hLegends.append(l)

cLoss = TCanvas("cLoss", "cLoss", 800, 800)
cLoss.Divide(2,2)
for i in range(4):
    p = cLoss.cd(i+1)
    p.SetGridx()
    p.SetGridy()
    #p.SetLogy()

    hFrames[i].Draw()

colors = [kBlue+1, kRed+1, kGreen+4,
          kMagenta+3, kOrange-2, kCyan+3, 
          kViolet+2, kOrange+2, kTeal+3,
          kBlack, kGray]
for ii, key in enumerate(grps.keys()):
    for i, g in enumerate(grps[key]):
        color = colors[ii]

        g.SetLineColor(color)
        g.SetLineWidth(2)
        g.SetMarkerColor(color)
        g.SetMarkerStyle(kFullCircle)
        g.SetMarkerSize(0.6)

        cLoss.cd(i+1)
        if g.GetN() > 0: g.Draw("PLsame")

        hLegends[i].AddEntry(g)

for i in range(4):
    p = cLoss.cd(i+1)
    hLegends[i].Draw()

